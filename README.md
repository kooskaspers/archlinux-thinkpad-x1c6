# archlinux-thinkpad-x1c6
guide for installing arch on a ThinkPad X1C6
---
### preparing bootable USB drive
Assuming we are running *unix*, ready to write the arch iso to a USB drive.
find your usb drive and unmount it
```
lsblk
umount /dev/sdx
```
Write the iso to the USB drive
```bash
dd bs=4M if=/path/to/iso of=/path/to/usb status=progress oflag=sync
```
disable SAFE boot in the bios. Enable UEFI, disable anything with legacy boot. Enable Thunderbolt BIOS assist mode
reboot, select USB drive

### preparing the install
move a mirror close to your geographical location to the top of this file:

```bash
~/etc/pacman.d/mirrorlist
```

refresh pacman
```bash
pacman -Syu
```

configure wifi, if you need
```bash
wifi-menu
```

check if internet is working
```bash
ping 8.8.8.8
```
note: sometimes wifi-menu needs a few seconds to activate the connection

enable NTP
```bash
timedatectl set-ntp true
```

check if the system supports EFI
```bash
ls /sys/firmware/efi/
```
folder should exist

### partitioning the drive
get an overview of your drives
```bash
lsblk
```
in my case:
nvme0n1 is the SSD, nvme0npX are the partitions

we are going to use cfdisk for the partitioning:

```bash
cfdisk /dev/nvme0n1
```

make sure the label is set to 'gpt'.
If not set to gpt 
```bash
parted /dev/nvme0n1
mklabel gpt
``` 

create this structure:

device | size | type
--- | --- | ---
/dev/nvme0n1p1 | 300M | EFI System
/dev/nvme0n1p2 | 8G | Linux Swap
/dev/nvme0n1p3 | 32G | Linux root (x86-64)
/dev/nvme0n1p4 | rest of drive | Linux home

### create the filesystems
```bash
mkfs.fat -F32 /dev/nvme0n1p1
mkswap /dev/nvme0n1p2
mkfs.ext4 /dev/nvme0n1p3
mkfs.ext4 /dev/nvme0n1p4
```

### mount the filesystems and swap space
we will mount the EFI partition to /boot (This is the preferred method when directly booting a EFISTUB kernel from UEFI)
```bash
mount /dev/nvme0n1p3 /mnt
mkdir /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot
mkdir /mnt/home
mount /dev/nvme0n1p4 /mnt/home
swapon /dev/nvme0n1p2
```

### install the base system
```bash
pacstrap /mnt base base-devel
```

### generate the fstab file
```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

### chroot into the installed Arch Linux distribution
```bash
arch-chroot /mnt
```

### set the time-zone and update the hardware clock
```bash
ln -sf /usr/share/zoneinfo/REGION/CITY /etc/localtime
hwclock --systohc
```

### generate locale file
Uncomment “en_US.UTF-8 UTF-8” and other locales in /etc/locale.gen
run
```bash
locale-gen
```
and create the locale configuration
```bash
echo "LANG=en_US.UTF-8" > /etc/locale.conf
```

### set hostname
```bash
echo "HOSTNAME" > /etc/hostname
```

###add matching entries to hosts:
```bash
127.0.0.1	localhost
::1		localhost
127.0.1.1	myhostname.localdomain	myhostname
```

### set the root password
```bash
passwd
```

### install the bootloader
we are going to install GRUB as the bootloader and we make use of efibootmgr to write boot entries to NVRAM.
make sure the esp (EFI system partition) is mounted on /boot

install the packages grub and efibootmgr
```bash
pacman -S grub efibootmgr
```

the following command will install the GRUB EFI application grub64.efi to /boot/EFI/GRUB/ and install it's modules to /boot/grub/x86_64-efi/ and it will create an entry in the firmware boot manager, named GRUB

```bash
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
```

Use the grub-mkconfig tool to generate /boot/grub/grub.cfg
```bash
grub-mkconfig -o /boot/grub/grub.cfg
```

### install network stuff

```bash
pacman -S wpa_supplicant networkmanager network-manager-applet dialog
```

### reboot
exit the arch-chroot environment and reboot
```bash
exit
reboot
```
now you should be able to login into the newly installed arch system
use NetworkManager to setup a connection
```bash
nmcli device wifi -help
nmcli device wifi connect AP PASS
```

### install the X environment
```bash
pacman -S xorg xorg-server
```

### packages for intel microcode drivers, important
modify /etc/pacman.conf, comment this out:
```bash
[multilib]
Include = /etc/pacman.d/mirrorlist
```
install intel microcode
```bash
pacman -Sy intel-ucode
```

### install the LTS kernel
```bash
pacman -S linux-headers linux-lts linux-lts-headers
```

### add a user and set the password
```bash
useradd -m -g users -G wheel -s /bin/bash koos
passwd koos
```

### install a window manager
dwm, what else.
```bash
git clone https://git.suckless.org/dwm
sudo make clean install
```

### configure trackpad
I had issues with inputlib. just couldnt find the settings matching my needs. ended up by using the synaptics driver (xf86-input-synaptics). I apply settings upon boot, using [this](setTrackpadPreferences) script.

applied these settings in xfce4-mouse-settings:

![mouse1](images/mouse1.png?raw=true)
![mouse2](images/mouse2.png?raw=true)

For pixel perfect firefox scrolling, add this to the ENV variables of the xorg environment (in xinitrc for example)
```bash
MOZ_USE_XINPUT2=1
```
source: https://wiki.archlinux.org/index.php/Firefox/Tweaks#Pixel-perfect_trackpad_scrolling

### other stuff
```bash
pacman -Syu
git
dmenu
xorg-apps
xfce4-notifyd, it's nice
nitrogen
wireguard-lts (or wireguard-arch if on bleeding edge kernel)
wireguard-tools
xfce4-terminal
unzip
xbindkeys
redshift
albert
compton
clipmenud
python-pip
fzf
xf86-input-libinput
lib32-intel-dri
firefox
ranger
atool
```

### pretty fonts
https://www.reddit.com/r/archlinux/comments/5r5ep8/make_your_arch_fonts_beautiful_easily/

Install some fonts, for example:
```bash
  sudo pacman -S ttf-dejavu ttf-liberation noto-fonts
```


Enable font presets by creating symbolic links:
```bash
  sudo ln -s /etc/fonts/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d
  sudo ln -s /etc/fonts/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d
  sudo ln -s /etc/fonts/conf.avail/11-lcdfilter-default.conf /etc/fonts/conf.d
```

Enable FreeType subpixel hinting mode by editing:
```bash
  /etc/profile.d/freetype2.sh
```

Uncomment the desired mode at the end:
```bash
  export FREETYPE_PROPERTIES="truetype:interpreter-version=40"
```

For font consistency, all applications should be set to use the serif, sans-serif, and monospace aliases, which are mapped to particular fonts by fontconfig.

Create /etc/fonts/local.conf with following:
```xml
  <?xml version="1.0"?>
  <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
  <fontconfig>
      <match>
          <edit mode="prepend" name="family"><string>Noto Sans</string></edit>
      </match>
      <match target="pattern">
          <test qual="any" name="family"><string>serif</string></test>
          <edit name="family" mode="assign" binding="same"><string>Noto Serif</string></edit>
      </match>
      <match target="pattern">
          <test qual="any" name="family"><string>sans-serif</string></test>
          <edit name="family" mode="assign" binding="same"><string>Noto Sans</string></edit>
      </match>
      <match target="pattern">
          <test qual="any" name="family"><string>monospace</string></test>
          <edit name="family" mode="assign" binding="same"><string>Noto Mono</string></edit>
      </match>
  </fontconfig>
```

Set your font settings to match above in your DE system settings.
Note: You can use a font family of your choice, other than Noto fonts.

### configuring look & feel, DPI, GTK themes
A very convenient tool to adjust fonts and look & feel of GTK(2/3) applications is the xfce4-settings-manager and its companying daemon xfsettingsd.

```bash
pacman -Syu xfce4-settings
```
![Xfce4-settings-manager](images/xfce4-settings-manager.png?raw=true "xfce4-settings manager")

### bluetooth
Get bluetooth working with these packages:

```bash
sudo pacman -Syu bluez
sudo pacman -Syu bluez-utils
sudo pacman -Syu pulseaudio-bluetooth
```
enable and start the bluetooth.service

autopower on bluetooth after startup, put this in /etc/bluetooth/main.conf:
```bash
[Policy]
AutoEnable=true
```

enter the bluetoothctl interface with
```bash
bluetoothctl
```

bring bluetooth up with
```bash
power up
```

scan for devices
```bash
scan on
```

pair a device
```bash
pair [dev]
```

connect to a device
```bash
connect [dev]
```

to select a bluetooth speaker as an audio source, use pulsemixer. In pulsemixer, one can enable the High Fidelity Playback (A2DP Sink) in the Cards section. The bluetooth device can be set as the default output device as well.


### power-management
install tlp for better power management on thinkpads:

pacman -Syu tlp

```bash
enable and start service
sudo systemctl enable tlp-sleep.service
sudo systemctl start tlp-sleep.service
sudo systemctl enable tlp.service
sudo systemctl start tlp.service
```

check the recommendations:
```bash
sudo tlp-stat -b
```

if needed, install acpi_call, in case of lts kernel:
```bash
pacman -Syu acpi_call-lts
```

### systemd-timesyncd
setup timesyncd

https://wiki.archlinux.org/index.php/systemd-timesyncd

sources:
https://github.com/ejmg/an-idiots-guide-to-installing-arch-on-a-lenovo-carbon-x1-gen-6
https://wiki.archlinux.org/index.php/Installation_guide
https://www.fosslinux.com/7117/how-to-install-arch-linux-complete-guide.htm
